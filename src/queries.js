import gql from 'graphql-tag';

export const POST_QUERY = gql`
  query postQuery($orderBy: PostOrderByInput) {
    posts(orderBy: $orderBy) {
      count
      postList {
        id
        text
        isLike
        reviews {
          id
          text
        }
      }
    }
  }
`;

export const POST_POST_MUTATION = gql`
  mutation PostMutation($text: String!) {
    postPost(text: $text) {
      id
      text
      isLike
      reviews {
        id
        text
      }
    }
  }
`;

export const POST_REVIEW_MUTATION = gql`
  mutation PostMutation($postId: ID!, $text: String!) {
    postReview(postId: $postId, text: $text) {
      id
      text
    }
  }
`;

export const POST_REACTION_MUTATION = gql`
  mutation PostMutation($postId: ID!, $isLike: Int) {
    postReaction(postId: $postId, isLike: $isLike) {
      id
      isLike
    }
  }
`;

export const NEW_POST_SUBSCRIPTION = gql`
  subscription {
    newPost {
      id
      text
      isLike
      reviews {
        id
        text
      }
    }
  }
`;