import React from 'react';
import ReviewList from '../Review/ReviewList';
import { Mutation } from 'react-apollo';

import { POST_REACTION_MUTATION } from '../../queries';

const ProductItem = props => {
  const { id, text, reviews, isLike} = props;

  return (
    <div className="product-item">
      <div className="title-wrapper">
        <h2>{text}</h2>
        <div>{ isLike ? isLike : '0'}</div>
      </div>
      <ReviewList postId={id} reviews={reviews} />

      <Mutation
        mutation={POST_REACTION_MUTATION}
        variables={{ text, isLike: 1 }}

      >
        {postMutation =>
          <button onClick={postMutation}>Like</button>
        }
      </Mutation>
    </div>
  );
};

export default ProductItem;