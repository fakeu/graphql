import React, { useState } from 'react';
import { Mutation } from 'react-apollo';
import { POST_POST_MUTATION, POST_QUERY } from '../../queries';

const ProductForm = props => {
  const [text, setText] = useState('');

  const _updateStoreAfterAddingProduct = (store, newPost) => {
    const orderBy = 'createdAt_DESC';
    const data = store.readQuery({
      query: POST_QUERY,
      variables: {
        orderBy
      }
    });
    data.posts.postList.unshift(newPost);
    store.writeQuery({
      query: POST_QUERY,
      data,
    });
  };

  return (
    <div className="form-wrapper">
      <div className="input-wrapper">
        <input type="text" placeholder="Title" value={text} onChange={e => setText(e.target.value)} />
      </div>

      <Mutation
        mutation={POST_POST_MUTATION}
        variables={{ text }}
        update={(store, { data: { postPost } }) => {
          _updateStoreAfterAddingProduct(store, postPost);
        }}
      >
        {postMutation =>
          <button className="post-button" onClick={postMutation}>Add</button>
        }
      </Mutation>
    </div>
  );
};

export default ProductForm;