import React from 'react';
import { Query } from 'react-apollo';
import ProductItem from '../Product/ProductItem';
import { POST_QUERY, NEW_POST_SUBSCRIPTION } from '../../queries';

import ProductForm from './ProductForm';

const ProductList = props => {
  const orderBy = 'createdAt_DESC';

  const _subscribeToNewProducts = subscribeToMore => {
    subscribeToMore({
      document: NEW_POST_SUBSCRIPTION,
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;
        const { newPost } = subscriptionData.data;
        const exists = prev.posts.postList.find(({ id }) => id === newPost.id);
        if (exists) return prev;

        return {...prev, posts: {
          postList: [newPost, ...prev.posts.postList],
          count: prev.posts.postList.length + 1,
          __typename: prev.posts.__typename
        }};
      }
    });
  };

  return (
    <Query query={POST_QUERY} variables={{ orderBy }}>
      {({ loading, error, data, subscribeToMore }) => {
        if (loading) return <div>Loading...</div>;
        if (error) return <div>Fetch error</div>;
        _subscribeToNewProducts(subscribeToMore);

        const { posts: { postList } } = data;
        console.log(postList);
        return (
          <div className="product-list">
            {postList.map(item => {
              return <ProductItem key={item.id} {...item} />
            })}
          </div>
        );
      }}
    </Query>
  );
};

export default ProductList;