function reviews(parent, args, context) {
    return context.prisma.post({
        id: parent.id
    }).reviews();
}

module.exports = {
    reviews
}