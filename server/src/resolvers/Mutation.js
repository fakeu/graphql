function postPost(parent, args, context, info) {
  return context.prisma.createPost({
    text: args.text
  });
}

async function postReview(parent, args, context, info) {
  const postExists = await context.prisma.$exists.post({
    id: args.postId
  });

  if (!postExists) {
    throw new Error(`Post with ID ${args.postId} does not exist`);
  }

  return context.prisma.createReview({
    text: args.text,
    post: { connect: { id: args.postId } }
  });
}

async function postReaction(parent, args, context, info) {
  const postExists = await context.prisma.$exists.post({
    id: args.postId
  });

  if (!postExists) {
    throw new Error(`Post with ID ${args.postId} does not exist`);
  }

  return context.prisma.updatePost({
    isLike: args.isLike
  });
}

module.exports = {
  postPost,
  postReview,
  postReaction
}