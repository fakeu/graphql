function post(parent, args, context) {
    return context.prisma.review({
        id: parent.id
    }).post();
}

module.exports = {
    post
}