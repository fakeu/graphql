const { GraphQLServer } = require('graphql-yoga');
const { prisma } = require('./generated/prisma-client');
const Query = require('./resolvers/Query');
const Mutation = require('./resolvers/Mutation');
const Subscription = require('./resolvers/Subscription');
const Review = require('./resolvers/Review');
const Post = require('./resolvers/Post');

const resolvers = {
  Query,
  Mutation,
  Subscription,
  Review,
  Post
};

const server = new GraphQLServer({
  typeDefs: './server/src/schema.graphql',
  resolvers,
  context: request => {
    return {
      ...request, prisma
    };
  }
});

server.start(() => console.log('http://localhost:4000'));